import React from "react";
import { useState } from "react";
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  StatusBar,
} from "react-native";
import { COLORS, FONTS, icons, SIZES } from "../constants";
import { RNCamera, FaceDetector } from "react-native-camera";

const Home = () => {
  const [currentLocation, setCurrentLocation] = useState({
    streetName: "Kuching",
    gps: {
      latitude: 1.5496614931250685,
      longitude: 110.36381866919922,
    },
  });

  const initialCurrentLocation = {
    streetName: "Kuching",
    gps: {
      latitude: 1.5496614931250685,
      longitude: 110.36381866919922,
    },
  };
  // price rating
  const affordable = 1;
  const fairPrice = 2;
  const expensive = 3;

  const renderHeader = () => {
    return (
      <View style={{ flexDirection: "row", height: 50 }}>
        <TouchableOpacity
          style={{
            width: 50,
            paddingLeft: SIZES.padding * 2,
            justifyContent: "center",
          }}
        >
          <Image
            source={icons.nearby}
            resizeMode="contain"
            style={{
              width: 30,
              height: 30,
            }}
          />
        </TouchableOpacity>
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <View
            style={{
              width: "70%",
              height: "100%",
              backgroundColor: COLORS.lightGray3,
              alignItems: "center",
              justifyContent: "center",
              borderRadius: SIZES.radius,
            }}
          >
            <Text style={{ ...FONTS.h3 }}> {currentLocation.streetName} </Text>
          </View>
        </View>
        <TouchableOpacity
          style={{
            paddingRight: SIZES.padding * 2,
            width: 50,
            justifyContent: "center",
          }}
        >
          <Image
            source={icons.basket}
            resizeMode="contain"
            style={{
              width: 30,
              height: 30,
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      {renderHeader()}
      <View>
        <Text
          style={{
            textAlign: "center",
            fontSize: 24,
          }}
        >
          Welcome To Our App
        </Text>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightGray4,
    overflow: "hidden",
    paddingTop: StatusBar.currentHeight,
  },
  shadow: {
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      heght: 3,
    },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    elevation: 1,
  },
});
export default Home;
